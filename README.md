# Rock, Paper, Scissors #

Created by Steve Hayter, 11th May 2015

## Description ##

Simple C# console app version of the rock, paper, scissors game created to show my coding abilities/standards. There are 2 main projects in the solution:

*  **SteveHayter.RockPaperScissors.ConsoleApp**: Console application with main UI logic
*  **SteveHayter.RockPaperScissors.GameEngine**: Class library with main game engine logic/utility classes
*  **SteveHayter.RockPaperScissors.Tests**: Unit test project

Much of the game engine has been built in a modular way, using SOLID principles. As a result, much of it can be unit tested. I have only created some unit tests (mainly for game logic, and option parsing), but more could be created.

If I spent more time on it, I would probably break the UI down to be more generic, so it could be made into a Windows app, for example. But I prioritised the game logic and functionality over this.

Other improvements I'd potentially like to integrate in the future:

* Player versus player mode (ie: 2 human players)
* Logging of each round, with scores.
* Improved user interface/menu navigation.

To run, make the console application the startup application, and run in Visual Studio.