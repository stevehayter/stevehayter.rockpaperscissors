﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SteveHayter.RockPaperScissors.GameEngine.Games;
using SteveHayter.RockPaperScissors.GameEngine.Shapes;

namespace SteveHayter.RockPaperScissors.Tests
{
    [TestClass]
    public partial class Tests
    {
        [TestMethod]
        public void TestRockAndRock()
        {
            var resultGenerator = new GameResultGenerator(Shape.Rock, Shape.Rock);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Draw);
        }

        [TestMethod]
        public void TestRockAndPaper()
        {
            var resultGenerator = new GameResultGenerator(Shape.Rock, Shape.Paper);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Player2Wins);
        }

        [TestMethod]
        public void TestRockAndScissors()
        {
            var resultGenerator = new GameResultGenerator(Shape.Rock, Shape.Scissors);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Player1Wins);
        }

        [TestMethod]
        public void TestPaperAndPaper()
        {
            var resultGenerator = new GameResultGenerator(Shape.Paper, Shape.Paper);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Draw);
        }

        [TestMethod]
        public void TestPaperAndRock()
        {
            var resultGenerator = new GameResultGenerator(Shape.Paper, Shape.Rock);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Player1Wins);
        }

        [TestMethod]
        public void TestPaperAndScissors()
        {
            var resultGenerator = new GameResultGenerator(Shape.Paper, Shape.Scissors);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Player2Wins);
        }

        [TestMethod]
        public void TestScissorsAndScissors()
        {
            var resultGenerator = new GameResultGenerator(Shape.Scissors, Shape.Scissors);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Draw);
        }

        [TestMethod]
        public void TestScissorsAndRock()
        {
            var resultGenerator = new GameResultGenerator(Shape.Scissors, Shape.Rock);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Player2Wins);
        }

        [TestMethod]
        public void TestScissorsAndPaper()
        {
            var resultGenerator = new GameResultGenerator(Shape.Scissors, Shape.Paper);
            Assert.IsTrue(resultGenerator.GenerateResult() == GameResult.Player1Wins);
        }
    }
}
