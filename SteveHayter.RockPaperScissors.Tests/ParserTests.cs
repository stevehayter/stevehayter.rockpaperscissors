﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SteveHayter.RockPaperScissors.GameEngine.Modes;

namespace SteveHayter.RockPaperScissors.Tests
{
    public partial class Tests
    {
        [TestMethod]
        public void TestGameModePlayerVersusComputer()
        {
            var parser = new GameModeParser();
            Assert.IsTrue(parser.Parse('1') == GameMode.PlayerVersusComputer);
        }

        [TestMethod]
        public void TestGameModeComputerVersusComputer()
        {
            var parser = new GameModeParser();
            Assert.IsTrue(parser.Parse('2') == GameMode.ComputerVersusComputer);
        }

        [TestMethod]
        public void TestGameModeInvalidOption()
        {
            var parser = new GameModeParser();
            Assert.IsTrue(parser.Parse('X') == null);
        }
    }
}
