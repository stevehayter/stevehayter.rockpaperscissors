﻿using SteveHayter.RockPaperScissors.GameEngine.Games;

namespace SteveHayter.RockPaperScissors.GameEngine.Shapes
{
    internal class ShapeMatrixItem
    {
        internal Shape Player1Shape { get; set; }
        internal Shape Player2Shape { get; set; }
        internal GameResult Result { get; set; }
    }
}
