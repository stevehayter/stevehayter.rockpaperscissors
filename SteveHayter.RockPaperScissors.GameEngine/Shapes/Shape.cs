﻿namespace SteveHayter.RockPaperScissors.GameEngine.Shapes
{
    public enum Shape
    {
        Rock,
        Paper,
        Scissors
    }
}
