﻿using SteveHayter.RockPaperScissors.GameEngine.Games;

namespace SteveHayter.RockPaperScissors.GameEngine.Shapes
{
    internal static class ShapeMatrix
    {
        internal static ShapeMatrixItem[] ResultItems =
        {
            // Rock primary
            new ShapeMatrixItem() { Player1Shape = Shape.Rock, Player2Shape = Shape.Rock, Result = GameResult.Draw },
            new ShapeMatrixItem() { Player1Shape = Shape.Rock, Player2Shape = Shape.Paper, Result = GameResult.Player2Wins },
            new ShapeMatrixItem() { Player1Shape = Shape.Rock, Player2Shape = Shape.Scissors, Result = GameResult.Player1Wins },

            // Paper primary
            new ShapeMatrixItem() { Player1Shape = Shape.Paper, Player2Shape = Shape.Rock, Result = GameResult.Player1Wins },
            new ShapeMatrixItem() { Player1Shape = Shape.Paper, Player2Shape = Shape.Paper, Result = GameResult.Draw },
            new ShapeMatrixItem() { Player1Shape = Shape.Paper, Player2Shape = Shape.Scissors, Result = GameResult.Player2Wins },

            // Scissors primary
            new ShapeMatrixItem() { Player1Shape = Shape.Scissors, Player2Shape = Shape.Rock, Result = GameResult.Player2Wins },
            new ShapeMatrixItem() { Player1Shape = Shape.Scissors, Player2Shape = Shape.Paper, Result = GameResult.Player1Wins },
            new ShapeMatrixItem() { Player1Shape = Shape.Scissors, Player2Shape = Shape.Scissors, Result = GameResult.Draw },
        };

        internal static Shape[] ComputerItems = new[]
        {
            // Populate with 3 groups of the items so the randomisation is fairer
            Shape.Rock,
            Shape.Paper,
            Shape.Scissors,
            Shape.Rock,
            Shape.Paper,
            Shape.Scissors,
            Shape.Rock,
            Shape.Paper,
            Shape.Scissors
        };

    }
}
