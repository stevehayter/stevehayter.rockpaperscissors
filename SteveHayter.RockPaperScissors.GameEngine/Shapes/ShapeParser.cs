﻿namespace SteveHayter.RockPaperScissors.GameEngine.Shapes
{
    public class ShapeParser
    {
        public Shape? Parse(char shape)
        {
            if (shape == 'r' || shape == 'R')
            {
                return Shape.Rock;
            }
            else if (shape == 's' || shape == 'S')
            {
                return Shape.Scissors;
            }
            else if (shape == 'p' || shape == 'P')
            {
                return Shape.Paper;
            }

            return null;
        }
    }
}
