﻿namespace SteveHayter.RockPaperScissors.GameEngine.Modes
{
    public enum GameMode
    {
        PlayerVersusComputer,
        ComputerVersusComputer,
        PlayerVersusPlayer, // For future use
        Exit
    }
}
