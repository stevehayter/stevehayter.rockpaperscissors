﻿namespace SteveHayter.RockPaperScissors.GameEngine.Modes
{
    public class GameModeParser
    {
        public GameMode? Parse(char mode)
        {
            if (mode == '1')
            {
                return GameMode.PlayerVersusComputer;
            }
            else if (mode == '2')
            {
                return GameMode.ComputerVersusComputer;
            }
            else if (mode == '3')
            {
                return GameMode.Exit;
            }

            return null;
        }
    }
}
