﻿using SteveHayter.RockPaperScissors.GameEngine.Games;
using SteveHayter.RockPaperScissors.GameEngine.Shapes;
using System;
using System.Linq;
using System.Threading;

namespace SteveHayter.RockPaperScissors.GameEngine
{
    /// <summary>
    /// Represents a base game object.
    /// </summary>
    public abstract class Game
    {
        public Shape Player1Shape { protected set; get; }
        public string Player1Description { protected set; get; }
        public Shape Player2Shape { protected set; get; }
        public string Player2Description { protected set; get; }

        /// <summary>
        /// Gets a random shape.
        /// </summary>
        /// <returns>Shape</returns>
        protected Shape GetRandomShape()
        {
            var random = new Random();
            
            // Generate random number between 1 and shape length
            var shapeIndex = random.Next(0, ShapeMatrix.ComputerItems.Count() - 1);

            // Now sleep for an indetermine amount of time between 1 and 500 milliseconds so the seed varies
            Thread.Sleep(random.Next(1, 500));

            // Get index from array
            return ShapeMatrix.ComputerItems[shapeIndex];
        }

        /// <summary>
        /// Plays the game, generating a result.
        /// </summary>
        /// <returns>Result</returns>
        public GameResult Play()
        {
            // Get from generator
            var generator = new GameResultGenerator(Player1Shape, Player2Shape);

            // Get result
            return generator.GenerateResult();
        }
    }
}
