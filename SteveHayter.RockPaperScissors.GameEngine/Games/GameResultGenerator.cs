﻿using SteveHayter.RockPaperScissors.GameEngine.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteveHayter.RockPaperScissors.GameEngine.Games
{
    /// <summary>
    /// Compares 2 shapes, generating a result.
    /// </summary>
    public class GameResultGenerator
    {
        public Shape _player1Shape;
        public Shape _player2Shape;

        public GameResultGenerator(Shape player1Shape, Shape player2Shape)
        {
            _player1Shape = player1Shape;
            _player2Shape = player2Shape;
        }

        public GameResult GenerateResult()
        {
            // Get from matrix items (single throws exception if not found)
            var matrixItem = ShapeMatrix.ResultItems.Single(i => i.Player1Shape == _player1Shape &&
                                                                 i.Player2Shape == _player2Shape);

            // Get result
            return matrixItem.Result;
        }
    }
}
