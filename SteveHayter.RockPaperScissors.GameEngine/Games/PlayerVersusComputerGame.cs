﻿using SteveHayter.RockPaperScissors.GameEngine.Shapes;

namespace SteveHayter.RockPaperScissors.GameEngine.Games
{
    /// <summary>
    /// Represents a player versus computer game.
    /// </summary>
    public class PlayerVersusComputerGame : Game
    {
        public PlayerVersusComputerGame(Shape shapePlayer1)
        {
            Player1Shape = shapePlayer1;
            Player1Description = "player";
            Player2Shape = GetRandomShape();
            Player2Description = "computer";
        }
    }
}
