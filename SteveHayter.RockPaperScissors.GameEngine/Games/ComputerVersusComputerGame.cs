﻿namespace SteveHayter.RockPaperScissors.GameEngine.Games
{
    /// <summary>
    /// Represents a computer versus computer game.
    /// </summary>
    public class ComputerVersusComputerGame : Game
    {
        public ComputerVersusComputerGame()
        {
            Player1Shape = GetRandomShape();
            Player1Description = "computer";
            Player2Shape = GetRandomShape();
            Player2Description = "computer";
        }
    }
}
