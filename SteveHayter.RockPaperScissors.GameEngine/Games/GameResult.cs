﻿namespace SteveHayter.RockPaperScissors.GameEngine.Games
{
    public enum GameResult
    {
        Player1Wins,
        Player2Wins,
        Draw
    }
}
