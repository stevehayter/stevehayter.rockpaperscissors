﻿using SteveHayter.RockPaperScissors.GameEngine;
using SteveHayter.RockPaperScissors.GameEngine.Games;
using SteveHayter.RockPaperScissors.GameEngine.Modes;
using SteveHayter.RockPaperScissors.GameEngine.Shapes;
using System;

namespace SteveHayter.RockPaperScissors.ConsoleApp
{
    class Program
    {
        /// <summary>
        /// Main game console
        /// </summary>
        /// <param name="args">Console arguments</param>
        static void Main(string[] args)
        {
            Console.WriteLine("ROCK PAPER, SCISSORS");
            Console.WriteLine("-------------------------------------");

            GameMode? gameMode = null;

            do
            {
                // Display game menu
                gameMode = DisplayGameMenu();

                // Whilst we aren't existing the game
                if (gameMode != GameMode.Exit)
                {
                    Game game = null;
                    var cancelPlay = false;

                    // Whether we're player v computer, or computer v computer
                    switch (gameMode)
                    {
                        case GameMode.PlayerVersusComputer:
                        {
                            // We're player v computer, so we need to show a menu allowing the user
                            // to select a shape
                            Shape? shape = DisplayShapeMenu();

                            // Null shape, meaning we should cancel play and go back to game mode screen
                            if (shape == null)
                            {
                                // Cancel play
                                cancelPlay = true;
                            }
                            else
                            {
                                // Create new instance of player v computer game
                                game = new PlayerVersusComputerGame(shape.Value);
                            }
                            break;
                        }
                        case GameMode.ComputerVersusComputer:
                        {
                            // Computer v computer - we don't need any more information. Just create a new
                            // instance of a computer v computer game.
                            game = new ComputerVersusComputerGame();
                            break;
                        }
                    }

                    // If not cancelling play
                    if (!cancelPlay)
                    {
                        // Play the game
                        var result = game.Play();

                        // Display results
                        DisplayResults(game, result);
                    }
                }
            }
            while (gameMode != GameMode.Exit);
        }

        /// <summary>
        /// Displays the game menu.
        /// </summary>
        /// <returns>Selected game mode.</returns>
        static GameMode DisplayGameMenu()
        {
            Console.WriteLine("");
            Console.WriteLine("Select a game mode");

            Console.WriteLine("----------------------------------");

            Console.WriteLine("1. Person Versus Computer");
            Console.WriteLine("2. Computer Versus Computer");
            Console.WriteLine("3. Exit Game");

            Console.WriteLine("");
            Console.WriteLine("Select:");

            var gameModeString = Console.ReadKey().KeyChar;
            Console.WriteLine("");

            var gameModeParser = new GameModeParser();
            var gameMode = gameModeParser.Parse(gameModeString);

            while (gameMode == null)
            {
                Console.WriteLine("Please type a valid option (either 1, 2, or 3)");
                Console.WriteLine("Select:");
                gameModeString = Console.ReadKey().KeyChar;
                gameMode = gameModeParser.Parse(gameModeString);
                Console.WriteLine("");
            }

            return gameMode.Value;
        }

        /// <summary>
        /// Displays the shape selection menu.
        /// </summary>
        /// <returns>Selected shape option, or null if return to menu was selected.</returns>
        static Shape? DisplayShapeMenu()
        {
            Console.WriteLine("");
            Console.WriteLine("Please type a valid option (either 1, 2, or 3)");
            Console.WriteLine("");
            Console.WriteLine("Select a shape");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("Type 'r' for Rock");
            Console.WriteLine("Type 'p' for Paper");
            Console.WriteLine("Type 's' for Scissors");
            Console.WriteLine("Type 'm' to go back to menu");

            Console.WriteLine("");
            Console.WriteLine("Select:");

            var shapeString = Console.ReadKey().KeyChar;
            Console.WriteLine("");

            if (shapeString == 'm' || shapeString == 'M')
            {
                return null;
            }

            var shapeParser = new ShapeParser();
            var shape = shapeParser.Parse(shapeString);
            while (shape == null)
            {
                Console.WriteLine("Please type a valid option (either 1, 2, or 3)");
                Console.WriteLine("Select:");
                shapeString = Console.ReadKey().KeyChar;
                shape = shapeParser.Parse(shapeString);
                Console.WriteLine("");
            }

            return shape;
        }

        /// <summary>
        /// Displays the game results.
        /// </summary>
        /// <param name="game">Game instance.</param>
        /// <param name="result">Game instance result.</param>
        static void DisplayResults(Game game, GameResult result)
        {
            Console.WriteLine("----------------------------------");
            Console.WriteLine(String.Format("Player 1 ({0}) selected " + game.Player1Shape.ToString(), game.Player1Description));
            Console.WriteLine(String.Format("Player 2 ({0}) selected " + game.Player2Shape.ToString(), game.Player2Description));
            Console.WriteLine("----------------------------------");

            switch (result)
            {
                case GameResult.Player1Wins:
                    {
                        Console.WriteLine("PLAYER 1 WINS!");
                        break;
                    }
                case GameResult.Player2Wins:
                    {
                        Console.WriteLine("PLAYER 2 WINS!");
                        break;
                    }
                case GameResult.Draw:
                    {
                        Console.WriteLine("DRAW!");
                        break;
                    }
            }

            Console.WriteLine("----------------------------------");
        }
    }
}
